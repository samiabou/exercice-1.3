package fr.cnam.foad.nfa035.fileutils.streaming.test;

import fr.cnam.foad.nfa035.fileutils.streaming.media.ImageByteArrayFrame;
import fr.cnam.foad.nfa035.fileutils.streaming.serializer.ImageDeserializerBase64StreamingImpl;
import fr.cnam.foad.nfa035.fileutils.streaming.serializer.ImageSerializerBase64StreamingImpl;
import fr.cnam.foad.nfa035.fileutils.streaming.serializer.ImageStreamingDeserializer;
import fr.cnam.foad.nfa035.fileutils.streaming.serializer.ImageStreamingSerializer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;


/**
 *
 * Classe de Test unitaire Streaming Log4j2
 */


public class StreamingLog4j2Test {

    /**
     * Test unitaire modifié avec Log4j2
     *
     * @param args
     */
    public static void main(String[] args) {

        try {


            File image = new File("petite_image_2.png");
            File image2 = new File("superman.jpg");
            ImageByteArrayFrame media = new ImageByteArrayFrame(new ByteArrayOutputStream());

            /**
             * Transformer l'ensemble des sorties console (System.out.println) 
             * en appels Log4j
             * 
             */
            // Sérialisation
            ImageStreamingSerializer serializer = new ImageSerializerBase64StreamingImpl();
            serializer.serialize(image, media);

            String encodedImage = media.getEncodedImageOutput().toString();
            System.out.println(encodedImage + "\n");
            Logger log = LogManager.getLogger();
            log.info(" test d'affichage de la chaîne de notre image codée en base64");

            // Désérialisation
            ByteArrayOutputStream deserializationOutput = new ByteArrayOutputStream();
            ImageStreamingDeserializer deserializer = new ImageDeserializerBase64StreamingImpl(deserializationOutput);

            deserializer.deserialize(media);
            byte[] deserializedImage = ((ByteArrayOutputStream)deserializer.getSourceOutputStream()).toByteArray();
            // Vérification
            // 1/ Automatique
            byte[] originImage = Files.readAllBytes(image.toPath());
            byte[] originImage2 = Files.readAllBytes(image2.toPath());
            // assert Arrays.equals(originImage, deserializedImage);
            try {
                assert (Arrays.equals(originImage,originImage2));
                log.info(" Cette sérialisation est bien réversible :)");
            } catch (AssertionError e){
                log.error("Erreur: L'image originale ne correspond pas à l'image désérialisée et décodée ");
            }
            assert (!Arrays.equals(originImage,originImage2));
            log.fatal("Erreur FATAL : Cette sérialisation n'est pas réversible");

            //  2/ Manuelle
            File extractedImage = new File("petite_image_extraite.png");
            if (extractedImage == null){

                log.fatal("Erreur FATAL : L'image extraite est à NULL!");
            }else {

                new FileOutputStream(extractedImage).write(deserializedImage);
                log.info("Je peux vérifier moi-même en ouvrant mon navigateur de fichiers et en ouvrant l'image extraite dans le répertoire de ce Test");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String difference(String str1, String str2) {
        if (str1 == null) {
            return str2;
        }
        if (str2 == null) {
            return str1;
        }
        int at = indexOfDifference(str1, str2);
        if (at == -1) {
            return "";
        }
        return str2.substring(at);
    }

    public static int indexOfDifference(CharSequence cs1, CharSequence cs2) {
        if (cs1 == cs2) {
            return -1;
        }
        if (cs1 == null || cs2 == null) {
            return 0;
        }
        int i;
        for (i = 0; i < cs1.length() && i < cs2.length(); ++i) {
            if (cs1.charAt(i) != cs2.charAt(i)) {
                break;
            }
        }
        if (i < cs2.length() || i < cs1.length()) {
            return i;
        }
        return -1;
    }

}


